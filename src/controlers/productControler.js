'use-strict';
const mongoose = require('mongoose');
const Product = mongoose.model('Product');

exports.get = (req, res, next) => {
    Product
    .find({
        active: true,
    }, 'title description price slug')
    .then(data => {
        res.status(200).send(data);
    }).catch(e => { 
        res.status(400).send(e);
    });
}

exports.post = (req, res, next) => {
    let product = new Product(req.body);
    product
        .save()
        .then(x => {
            res.status(201).send({message: "produto cadastrado!"});
        }).catch(e => { 
            res.status(400).send({
                message: "falha ao cadastrar o produto",
                data: e
            });
        });
};

exports.delete = (req, res, next) => {
    res.status(200).send(req.body);
};

exports.put = (req, res, next) => {
    Product
        .findByIdAndUpdate(req.params.id, {
            $set: {
                title: req.body.title,
                description: req.body.description,
                price: req.body.price,

            }
        })
        .then(x => {
            res.status(200).send({message: "produto atualizado!"});
        }).catch(e => { 
            res.status(400).send({
                message: "falha ao atualizar o produto",
                data: e
            });
        });
}