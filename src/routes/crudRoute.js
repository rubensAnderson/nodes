const express = require('express');
const controler = require('../controlers/productControler');
const router = express.Router();

router.get('/', controler.get);
router.post('/', controler.post);
router.put('/:id', controler.put);
router.delete('/', controler.delete);

module.exports = router;