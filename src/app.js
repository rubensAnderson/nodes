'use-strict'

const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const config = require('./config');

const md5 = req('md5');

const app = express();


mongoose.connect(config.conectionString, { useNewUrlParser: true });

const Product = require('./model/product');
// routes
const indexRoute = require('./routes/indexRoute');
const crud = require('./routes/crudRoute');

app.use(bodyParser.json()); // converte todo o conteudo para json
app.use(bodyParser.urlencoded({ extended: false })); // codificar a url 

app.use('/', indexRoute); // para novas rotas, colocar coisas depois da barra
app.use('/products', crud);

module.exports = app;